TESTS=test/*.cpp
SRC=src/*.cpp
HEADERS=src/*.h

test: $(TESTS) $(SRC) $(HEADERS)
	set -e; \
	bundle exec arduino_ci.rb --skip-examples-compilation;

clean:
	set -e; \
	rm -rf *.bin *.dSYM *.lock;
