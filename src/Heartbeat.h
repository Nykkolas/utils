#ifndef HEARTBEAT_H
#define HEARTBEAT_H

#include "Arduino.h"

class Heartbeat {
    private:
        long _delay;
        long _lastHeartbeat;
    
    public:
        Heartbeat();
        void begin(long delay);
        void iterate();
};

#endif