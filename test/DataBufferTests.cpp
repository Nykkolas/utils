#include <ArduinoUnitTests.h>
#include <Godmode.h>
#include <DataBuffer.h>

DataBuffer dataBuffer;

unittest_setup()
{
  dataBuffer.begin('$', '\n', 1000);
}

unittest(begin_ok) {
    GodmodeState* state = GODMODE();

    assertEqual("<Arduino ready to work !>\r\n", state->serialPort[0].dataOut);
}
  
unittest (buffer_empty_ok) {
    GodmodeState* state = GODMODE();
    String input = "$toto\n";
    String expected = "";
    state->serialPort[0].dataOut = "";
    state->serialPort[0].dataIn = input;
    int loopx;

    for (loopx = 0; loopx < 9; loopx++) {
        dataBuffer.iterate();
    }

    state->micros = 1500000;
    dataBuffer.iterate();

    assertEqual(expected, dataBuffer.getDataBuffer());
}

unittest (buffer_fill_ok) {
    GodmodeState* state = GODMODE();
    String input = "$toto\n";
    String expected = "$toto";
    state->serialPort[0].dataIn = input;
    int loopx;

    for (loopx = 0; loopx < 9; loopx++) {
        dataBuffer.iterate();
    }

    assertEqual(expected, dataBuffer.getDataBuffer());
}

unittest (isReceiving_buffer_filling) {
    GodmodeState* state = GODMODE();
    String input = "$toto\n";
    state->serialPort[0].dataIn = input;
    int loopx;

    for (loopx = 0; loopx < 2; loopx++) {
        dataBuffer.iterate();
    }

    assertTrue(dataBuffer.isReceiving());
}

unittest (isBufferOK_buffer_ok) {
    GodmodeState* state = GODMODE();
    String input = "$toto\n";
    state->serialPort[0].dataIn = input;
    int loopx;

    for (loopx = 0; loopx < 9; loopx++) {
        dataBuffer.iterate();
    }

    assertTrue(dataBuffer.isBufferOK());
}

String resultConcat;
void resultConcatCallback (char c) {
    resultConcat.concat(c);
}
unittest (process_char_callback) {
    GodmodeState* state = GODMODE();
    String input = "$toto\n";
    String expected = "$toto";
    state->serialPort[0].dataIn = input;
    int loopx;
    
    dataBuffer.setProcessCharCallback(resultConcatCallback);

    for (loopx = 0; loopx < 9; loopx++) {
        dataBuffer.iterate();
    }

    dataBuffer.setProcessCharCallback(NULL);

    assertEqual(expected, resultConcat);
}

char resultBuffer[84];
void resultBufferCallback (char buffer[]) {
    int i;
    for (i = 0; i < 84; i++) {
        resultBuffer[i] = buffer[i];
    }
}

unittest (process_buffer_callback) {
    GodmodeState* state = GODMODE();
    String input = "$toto\n";
    char expected[84] = {'$', 't', 'o', 't', 'o'};
    state->serialPort[0].dataIn = input;
    int loopx;
    
    /* 
     * Tentatives d'utilisation des lambda
     */
    // char resultBuffer[84];
    // auto resultBufferCallback = [&resultBuffer] (char buffer[]) {
    //     int i;
    //     for (i = 0; i < 84; i++) {
    //         resultBuffer[i] = buffer[i];
    //     }
    // };

    // dataBuffer.setProcessBufferCallback([resultBuffer] (char buffer[]) {
    //         int i;
    //         for (i = 0; i < 84; i++) {
    //             resultBuffer[i] = buffer[i];
    //         }
    //     });

    dataBuffer.setProcessBufferCallback(resultBufferCallback);

    for (loopx = 0; loopx < 9; loopx++) {
        dataBuffer.iterate();
    }

    dataBuffer.setProcessBufferCallback(NULL);

    assertEqual(expected, resultBuffer);
}

unittest_main()
