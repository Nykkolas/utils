Various utilities for Aduino development
- Heartbeat : write a string regularly in Serial
- LedBlink : make a led blink in a asynchronous way
- DataBuffer : read Serial character by character with start and end markers. Optional callbacks each character or when reading is finished