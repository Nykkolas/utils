#include <Arduino.h>
#include "DataBuffer.h"

// Ctors
DataBuffer::DataBuffer() {
    // Do Nothing
}

bool DataBuffer::isReceiving() {
  bool status = false;

  if (dataBufferState == BUFFER_FILLING) {
    status = true;
  }

  return status;
}

bool DataBuffer::isBufferOK() {
  bool status = false;

  if (dataBufferState == BUFFER_OK) {
    status = true;
  }

  return status;
};

void DataBuffer::begin(char _startChar, char _eofChar, long _waitStopDelay) {
  // init parameters
  startChar = _startChar;
  eofChar = _eofChar;
  waitStopDelay = _waitStopDelay;
  dbx = 0;
  processCharCallback = NULL;
  processBufferCallback = NULL;
  
  // initial state
  dataBufferState = BUFFER_EMPTY;

  // card conf
  Serial.println("<Arduino ready to work !>");
}

// inputs implementation

// outputs implementation
void DataBuffer::errorBufferFull() {
  Serial.println("[ERROR]Buffer is full. Buffer so far :");
  Serial.println("-------------");
  Serial.println(dataBuffer);
  Serial.println("-------------");
}

// transitions implementation
dataBufferStates DataBuffer::recvData() {
  char data;
  
  if (dbx == bufferSize) {
    errorBufferFull();
    stopRecvTime = millis();
    return BUFFER_FULL;
  }
  
  data = (char)Serial.read();
  if (data != eofChar) {
    addOneChar(data);
    return BUFFER_FILLING;
  } else {
    dataBuffer[dbx] = '\0';
    stopRecvTime = millis();
    if (processBufferCallback != NULL) {
      processBufferCallback(dataBuffer);
    }
//    Serial.println(dataBuffer);
    return BUFFER_OK;
  }
}

dataBufferStates DataBuffer::emptyBuffer() {
  dbx = 0;
  dataBuffer[0] = '\0';
  return BUFFER_EMPTY;
}

// internal functions
void DataBuffer::printDebug (char* variable) {
  Serial.print("[DEBUG]");
  Serial.println(variable);
}

void DataBuffer::addOneChar(char data) {
  dataBuffer[dbx] = data;
  if (*processCharCallback != NULL) {
    processCharCallback(data);
  }
  dbx++;
}

// main loop
void DataBuffer::iterate() {
  switch(dataBufferState) {
    case BUFFER_EMPTY:
      if (Serial.available() > 0) {
        char data = (char)Serial.read();
        if (data == startChar) {
          addOneChar(data);
          dataBufferState = BUFFER_FILLING;
        }
      }
      break;
    case BUFFER_FILLING:
      if (Serial.available() > 0) {
        dataBufferState = recvData();
      }
      break;
    case BUFFER_OK:
    case BUFFER_FULL:
      if ((millis() - stopRecvTime) > waitStopDelay) {
        dataBufferState = emptyBuffer();
      }
      break;
  }
}
