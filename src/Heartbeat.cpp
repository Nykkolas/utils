#include "Arduino.h"
#include "Heartbeat.h"

// Ctors
Heartbeat::Heartbeat () {
    // Do nothing
}

void Heartbeat::begin(long delay) {
    _delay = delay;
    _lastHeartbeat = millis();
}

void Heartbeat::iterate() {
    if (millis() - _lastHeartbeat > _delay) {
        Serial.println("<Bip>");
        _lastHeartbeat = millis();
    } 
}
