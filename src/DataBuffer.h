#ifndef DATABUFFER_H
#define DATABUFFER_H

#include <Arduino.h>

enum dataBufferStates {
  BUFFER_EMPTY,
  BUFFER_FILLING,
  BUFFER_OK,
  BUFFER_FULL
};

class DataBuffer {
  private:
    // parameters
    static const int bufferSize = 84; // Max NMEA0183 sentence size = 82 (source wikipedia)
    char startChar;
    char eofChar;
    long waitStopDelay;
    
    // state
    dataBufferStates dataBufferState;

    // inputs
    
    // outputs
    void errorBufferFull();
    
    // transitions
    dataBufferStates recvData();
    dataBufferStates stopRecvData();
    dataBufferStates emptyBuffer();
    
    // internal
    long stopRecvTime;
    int dbx;
    char dataBuffer[bufferSize + 1]; // +1 for \0 to end string
    void printDebug(char* variable);
    void addOneChar(char data);
    void (*processCharCallback)(char);
    void (*processBufferCallback)(char[]);
    // std::function<void(char [])> processBufferCallback;
    
  public:
    DataBuffer();
    void begin(char _startChar, char _eofChar, long _waitStopDelay);
    char* getDataBuffer() { return dataBuffer; };
    dataBufferStates getDataBufferState() { return dataBufferState; };
    void setProcessCharCallback (void (*_processCharCallback)(char)) { processCharCallback = _processCharCallback; };
    void setProcessBufferCallback (void (*_processBufferCallback)(char [])) { processBufferCallback = _processBufferCallback; };
    // void setProcessBufferCallback(std::function<void(char [])> _processBufferCallback) { processBufferCallback = _processBufferCallback; };

    bool isReceiving();
    bool isBufferOK();
    void iterate();
};

#endif
