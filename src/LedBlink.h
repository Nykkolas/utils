/*
 * Make a LED blink
 * 
 * Usage :
 * In setup : begin(pin, delay);
 * In loop : iterate();
 * 
 */

#ifndef LEDBLINK_H
#define LEDBLINK_H

#include "Arduino.h"

/*
 * Conservé pour donner un exemple d'utilisation de switch...case
 */
//enum blinkLedStates {
//  BLINK_LED_OFF,
//  BLINK_LED_ON
//};

class LedBlink {
  private:
    // parameters
    long led;
    long delay;

    // state
    int blinkLedState();
    // blinkLedStates blinkLedState;

    // transitions
    void switchLedOff();
    void switchLedOn();
    void toggleLedState();

    // input
    bool timeOut();
    
    // internal
    long startClock;
    void toggleLed();
    
  public:
    LedBlink();
    LedBlink(int _led, long _delay);
    void begin(int _led, long _delay);
    void iterate();
};

#endif
