#include "Arduino.h"
#include "LedBlink.h"

LedBlink::LedBlink() {
  // Do nothing
}

LedBlink::LedBlink(int _led, long _delay) {
  begin(_led, _delay);
}

void LedBlink::begin(int _led, long _delay) {
  // init parameters
  led = _led;
  delay = _delay;

  // initial state
  switchLedOff();

  // card conf
  pinMode (led, OUTPUT);
}

int LedBlink::blinkLedState() {
  return digitalRead(led);
}

void LedBlink::toggleLed() {
  digitalWrite(led, !blinkLedState());
  startClock = millis();
}

void LedBlink::switchLedOff() {
  digitalWrite(led,LOW);
  startClock = millis();
}

void LedBlink::switchLedOn() {
  digitalWrite(led, HIGH);
  startClock = millis();
}

bool LedBlink::timeOut() {
  if (millis() - startClock > delay) {
    return true;
  } else {
    return false;
  }
}

void LedBlink::iterate() {
  if (timeOut()) {
    toggleLed();
  }
/*
 * Conservé pour donner un exemple d'utilisation de switch...case
 */
//  switch(blinkLedState) {
//    case BLINK_LED_OFF:
//      if (timeOut()) {
//        blinkLedState = switchLedOn();
//      } 
//      break;
//    case BLINK_LED_ON:
//      if (timeOut()) {
//        blinkLedState = switchLedOff();
//      }
//      break;
//  }
}
